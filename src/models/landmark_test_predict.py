#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Model training 


:author: Ben Johnston

"""
import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from scipy.spatial.distance import euclidean
from src.models.landmark_models import model8
from src.data.data import POST_TRAIN_DIR
from src.data.data import LMRK_TRAIN_DIR, LMRK_VALID_DIR
from src.data.landmark_data import load_training_data, load_test_data

LMRK_TRAIN_DIR = os.path.join(POST_TRAIN_DIR, 'training')
TEST_SET_DIR ='/home/ben/Workspace/embc_2019_gan_masks/data/testset'
df = pd.read_csv('data/measurements.csv')
df = df[['Filename', 'Nose Width (mm)']]


_print = print

def print(msg):

    _print(msg)
    with open('progress.log', 'a') as f:
        f.write('%s\n' % msg)


def display_images(images, coords):
    for i in range(images.shape[0]):
        plt.subplot(4, 8, i + 1)
        plt.imshow(images[i])
        plt.scatter(coords[i,:,0], coords[i,:,1], s=1)
        plt.axis('off')
    plt.tight_layout()
    plt.savefig('predict.png')
    plt.close('all')


# Seed
np.random.seed(0)
tf.set_random_seed(0)

image_size = 50
learning_rate = 0.000000001
momentum = 0.9
batch_size = 10
n_iterations = 100000
n_epochs_print = 10

# Generator
mod = model8(image_size) 
mod.load_weights('mod_transfer.h5')


# Freeze the first two layers of the model
for i in range(5):
    mod.layers[i].trainable = False

mod.compile(
    optimizer=optimizers.SGD(lr=learning_rate, momentum=momentum),
    loss='mse')

_, _, y_mean, y_max = load_training_data(LMRK_TRAIN_DIR,
    image_size=image_size) 

xtest, yscale, filenames, resize_ratios = load_test_data(TEST_SET_DIR, image_size=image_size) 

y_pred = mod.predict(xtest)
y_pred *= y_max
y_pred += y_mean

y_pred = y_pred.reshape((-1, 2, 2))
xtest /= xtest.max()
xtest *= 255
xtest = xtest.astype(int)

# Convert scales to mm
scales = [28.65 / euclidean(y[0], y[1]) for y in yscale]
nose_widths = [euclidean(y[0], y[1]) for y in y_pred]

df_dict = {'Filename': [], 'Scale': [], 'Pred Width (px)': [], 'Resize':[]}
for img, coords, filename, scale, width, resize in zip(xtest, y_pred, filenames, scales, nose_widths, resize_ratios):
    plt.imshow(img)
    plt.scatter(coords[:,0], coords[:,1])
    plt.savefig('%s.jpg' % filename)
    plt.close()
    df_dict['Filename'].append(filename)
    df_dict['Scale'].append(scale)
    df_dict['Pred Width (px)'].append(width)
    df_dict['Resize'].append(resize)

df2 = pd.DataFrame(df_dict)
df = pd.merge(df, df2)

df['Pred Width'] = df['Scale'] * df['Pred Width (px)'] / df['Resize']
df['Diff'] = abs(df['Pred Width'] - df['Nose Width (mm)'])

def _size(x):

    if x < 37:
        return 'Small'
    elif (x >= 37) and (x < 41):
        return 'Medium'
    elif (x >= 41) and (x < 45):
        return 'Large'
    else:
        return 'Too Large'

df['True Size'] = [_size(x) for x in df['Nose Width (mm)']]
df['Pred Size'] = [_size(x) for x in df['Pred Width']]

print(sum(df['True Size'] == df['Pred Size']) / len(df))

df.to_csv('predictions.csv')

display_images(xtest[:32], y_pred[:32])
