#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Training data for GAN



:author: Ben Johnston

"""

# Imports
from src.data.data import RAW_COIN_DIR 
from keras.preprocessing.image import ImageDataGenerator


def get_training_data(data_path=RAW_COIN_DIR, target_size=(50, 50), batch_size=32, seed=0):

    return ImageDataGenerator(
        rescale=1./255,
#        horizontal_flip=True,
#        vertical_flip=True,
#        brightness_range=[0.9, 1.1],
        ).\
        flow_from_directory(
            data_path,
            class_mode=None,
            seed=seed,
            target_size=target_size,
    )
