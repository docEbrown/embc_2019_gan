#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Model training 


:author: Ben Johnston

"""
import os
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from src.models.landmark_models import model8
from src.data.data import POST_TRAIN_DIR
from src.data.data import LMRK_TRAIN_DIR, LMRK_VALID_DIR
from src.data.landmark_data import load_training_data

LMRK_TRAIN_DIR = os.path.join(POST_TRAIN_DIR, 'training')
LMRK_VALID_DIR = os.path.join(POST_TRAIN_DIR, 'valid')

_print = print

def print(msg):

    _print(msg)
    with open('progress.log', 'a') as f:
        f.write('%s\n' % msg)


def display_images(images, coords):
    for i in range(images.shape[0]):
        plt.subplot(4, 8, i + 1)
        plt.imshow(images[i])
        plt.scatter(coords[i,:,0], coords[i,:,1], s=1)
        plt.axis('off')
    plt.tight_layout()
    plt.savefig('predict.png')
    plt.close('all')



# Seed
np.random.seed(0)
tf.set_random_seed(0)

image_size = 50
learning_rate = 0.000000001
momentum = 0.9
batch_size = 10
n_iterations = 100000
n_epochs_print = 10

# Generator
mod = model8(image_size) 
mod.load_weights('mod_transfer.h5')

# Freeze the first two layers of the model
for i in range(5):
    mod.layers[i].trainable = False

mod.compile(
    optimizer=optimizers.SGD(lr=learning_rate, momentum=momentum),
    loss='mse')

xtrain, ytrain, ytrain_mean, ytrain_max, train_filenames  = load_training_data(LMRK_TRAIN_DIR,
    image_size=image_size, return_filenames=True) 

ytrain *= ytrain_max
ytrain += ytrain_mean

print('%d training samples' % len(xtrain))
print(mod.summary())

xvalid, yvalid, y_mean, y_max, valid_filenames = load_training_data(LMRK_VALID_DIR,
    image_size=image_size, return_filenames=True) 
#yvalid = yvalid.reshape((yvalid.shape[0], -1))

yvalid *= y_max
yvalid += y_mean

data_gen = ImageDataGenerator().flow(
    x=xtrain, y=ytrain, batch_size=batch_size)

y_pred = mod.predict(xvalid)
y_pred *= y_max
y_pred += y_mean

y_pred = y_pred.reshape((-1, 2, 2))
xvalid /= xvalid.max()
xvalid *= 255
xvalid = xvalid.astype(int)

for idx, filename in enumerate(valid_filenames):
    basename = os.path.basename(filename[0])
    plt.imshow(xvalid[idx])
    plt.scatter(y_pred[idx,:,0], y_pred[idx,: ,1])
    plt.scatter(yvalid[idx,:,0], yvalid[idx,: ,1], c='r', s=5)
    print(basename)
    plt.savefig(basename)
    plt.close()

y_pred = mod.predict(xtrain)
y_pred *= y_max
y_pred += y_mean
y_pred = y_pred.reshape((-1, 2, 2))

xtrain /= xtrain.max()
xtrain *= 255
xtrain = xtrain.astype(int)


for idx, filename in enumerate(train_filenames):
    basename = os.path.basename(filename[0])
    plt.imshow(xtrain[idx])
    plt.scatter(y_pred[idx,:,0], y_pred[idx,: ,1])
    plt.scatter(ytrain[idx,:,0], ytrain[idx,: ,1], c='r', s=5)
    print(basename)
    plt.savefig(basename)
    plt.close()
