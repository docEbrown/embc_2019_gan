#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Data


:author: Ben Johnston

"""

# Imports
import os
import random
import pickle
import numpy as np
import pandas as pd
from pathlib import Path
from PIL import Image

PROJECT_DIR = Path(__file__).resolve().parents[2]
DATA_DIR = os.path.join(PROJECT_DIR, 'data')
RAW_DATA_DIR = os.path.join(DATA_DIR, 'raw')
RAW_COIN_DIR = os.path.join(RAW_DATA_DIR, 'coins')
PROCESSED_DIR = os.path.join(DATA_DIR, 'processed')
NOSE_DATA_DIR = os.path.join(PROCESSED_DIR, 'noses')
LMRK_TRAIN_DIR = os.path.join(NOSE_DATA_DIR, 'training')
LMRK_VALID_DIR = os.path.join(NOSE_DATA_DIR, 'valid')
LMRK_TEST_DIR = os.path.join(PROCESSED_DIR, 'test')
POST_TRAIN_DIR = os.path.join(PROCESSED_DIR, 'post-train')
POST_TRAIN_TRAINING = os.path.join(POST_TRAIN_DIR, 'training')
POST_TRAIN_VALID = os.path.join(POST_TRAIN_DIR, 'valid')

random.seed(0)

def save_dat(name, dat_dict, dirname):

    name, _ = os.path.splitext(name)
    print(name)
    img = Image.fromarray(dat_dict['img'])
    img.save(os.path.join(dirname, '%s.jpg' % name))

    np.savetxt(os.path.join(dirname, '%s.txt' % name),
        dat_dict['resized_coords'])


def extract_data(pickle_file, training_dir, valid_dir, train_ratio=0.7):

    if not os.path.exists(training_dir):
        os.mkdir(training_dir)

    if not os.path.exists(valid_dir):
        os.mkdir(valid_dir)

    with open(pickle_file, 'rb') as f:
        dat = pickle.load(f)

    basenames = list(set([
        key if '_lr' not in key else key.replace('_lr', '') for key in dat.keys()]))
    random.shuffle(basenames)

    num_train = int(len(basenames) * train_ratio)
    train_names = basenames[:num_train]
    valid_names = basenames[num_train:]

    for name in train_names: save_dat(name, dat[name], training_dir)
    for name in valid_names: save_dat(name, dat[name], valid_dir)


if __name__ == "__main__":
#    extract_data(os.path.join(RAW_DATA_DIR, 'nnet_training_data.pkl'), 
#        LMRK_TRAIN_DIR, LMRK_VALID_DIR)

#    extract_data(os.path.join(RAW_DATA_DIR, 'nnet_post_train_data.pkl'), 
#        POST_TRAIN_TRAINING, POST_TRAIN_VALID)

    extract_data(os.path.join(RAW_DATA_DIR, 'nnet_test_data.pkl'), 
        LMRK_TEST_DIR, LMRK_TEST_DIR)
