#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Coin data



:author: Ben Johnston

"""

# Imports
import os
from src.data.data import RAW_DATA_DIR
from keras.preprocessing.image import ImageDataGenerator

RAW_COIN_DIR = os.path.join(RAW_DATA_DIR, 'coins') 

coin_generator = ImageDataGenerator(
    rescale=1/255.,
    horizontal_flip=True,
    vertical_flip=True,
    brightness_range=(0.8, 1.2),
    height_shift_range=2,
    width_shift_range=2,
#    zoom_range=(0.8, 1),
    rotation_range=60,
    )

dat = coin_generator.flow_from_directory(
    RAW_DATA_DIR,
    target_size=(100, 100),
    batch_size=8)


import matplotlib.pyplot as plt

x, y = dat.next()

for i in range(8):
    plt.subplot(1, 8, i + 1)
    plt.imshow(x[i])
    plt.axis('off')
plt.show()
