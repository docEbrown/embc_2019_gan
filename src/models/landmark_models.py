#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Landmark models


:author: Ben Johnston

"""

# Imports
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPooling2D, Dropout, Flatten, LeakyReLU


def model1(img_size):

    model = Sequential()
    model.add(Dense(200, input_shape=(img_size * img_size * 3,), activation='tanh'))
    model.add(Dense(4, activation='linear'))

    return model


def model2(img_size):

    model = Sequential()
    model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Flatten())

    model.add(Dense(200, activation='tanh'))
    model.add(Dense(4, activation='linear'))

    return model


def model3(img_size):

    model = Sequential()
    model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=64, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Flatten())

    model.add(Dense(200, activation='tanh'))
    model.add(Dense(4, activation='linear'))

    return model


def model4(img_size):

    model = Sequential()
    model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=64, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Flatten())

    model.add(Dense(500, activation='tanh'))
    model.add(Dense(200, activation='tanh'))
    model.add(Dense(4, activation='linear'))

    return model


def model5(img_size):

    model = Sequential()
    model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=64, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=128, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Flatten())

    model.add(Dense(500, activation='tanh'))
    model.add(Dense(200, activation='tanh'))
    model.add(Dense(4, activation='linear'))

    return model


def model6(img_size):

    model = Sequential()
    model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=64, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=128, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Flatten())

    model.add(Dense(500, activation='tanh'))
    model.add(Dropout(0.5))
    model.add(Dense(200, activation='tanh'))
    model.add(Dropout(0.25))
    model.add(Dense(4, activation='linear'))

    return model


def model7(img_size):

    model = Sequential()
    model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=64, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Conv2D(filters=128, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())

    model.add(Flatten())

    model.add(Dense(1000, activation='tanh'))
    model.add(Dense(500, activation='tanh'))
    model.add(Dense(250, activation='tanh'))
    model.add(Dense(4, activation='linear'))

    return model


def model8(img_size):

    model = Sequential()
    model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3), kernel_size=(3, 3)))
    model.add(LeakyReLU())
    model.add(MaxPooling2D())

    model.add(Conv2D(filters=64, kernel_size=(3, 3)))
    model.add(LeakyReLU())
    model.add(MaxPooling2D())

    model.add(Conv2D(filters=128, kernel_size=(3, 3)))
    model.add(LeakyReLU())
    model.add(MaxPooling2D())

    model.add(Conv2D(filters=256, kernel_size=(3, 3)))
    model.add(LeakyReLU())
    model.add(MaxPooling2D())

    model.add(Flatten())

    model.add(Dense(1000, activation='tanh'))
    model.add(Dropout(0.75))
    model.add(Dense(500, activation='tanh'))
    model.add(Dropout(0.5))
    model.add(Dense(250, activation='tanh'))
    model.add(Dropout(0.25))
    model.add(Dense(4, activation='linear'))

    return model

