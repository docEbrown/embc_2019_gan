#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Landmarking data



:author: Ben Johnston

"""

# Imports
import os
import random
import numpy as np
from imageio import imread
from skimage.transform import resize, rescale

import matplotlib.pyplot as plt


def load_training_data(data_dir, image_size=50, return_filenames=False):
    """Generate image / landmark pairs"""

    # Load the basenames from the directory
    basenames = []
    orig_filenames = []
    for filename in os.listdir(data_dir):
        orig_filenames.append(filename)
        basename, ext = os.path.splitext(filename)

        if ext != '.txt':
            basenames.append(basename)

    # Compile the images and landmark files into a list

    image_landmark_files = []
    for basename in basenames:
        pair = [None, None]
        for filename in orig_filenames:
            if basename in filename:
                pair['.txt' in filename] = os.path.join(data_dir, filename)
        if not None in pair:
            image_landmark_files.append(tuple(pair))


    # Load the data to feed into the generator
    x = []
    y = []
    for img_file, lmrk_file in image_landmark_files:
        img = imread(img_file)
        cols, rows, _ = img.shape
        lmrks = np.loadtxt(lmrk_file)
        _img = np.zeros((image_size, image_size, 3))
        _lmrks = np.copy(lmrks)

        # Just need to pad
        if (cols > image_size) or (rows > image_size):
            resize_rat = image_size / np.max(img.shape[:2])

            img = rescale(img, resize_rat, mode='constant')
            cols, rows, _ = img.shape
            _lmrks *= resize_rat 

        padding = abs(rows - image_size) // 2
        _img[:cols, padding:(rows + padding), :] = img
        _lmrks[:,0] += padding

        x.append(_img)
        y.append(_lmrks)

    # Rescale etc
    x = np.asarray(x)
    x /= 255.
    y = np.asarray(y)
    if 0:
        y_mean = y.mean()
        y -= y_mean 
        y_max = y.max()
        y /= y_max 

    if not return_filenames:
        return x, y,# y_mean, y_max
    else:
        return x, y, y_mean, y_max, image_landmark_files



def load_test_data(data_dir, image_size=50):
    """Generate image / landmark pairs"""

    # Load the data to feed into the generator
    x = []
    y = []
    filenames = []
    resize_ratios = []
    for img_file in os.listdir(data_dir):

        basename, ext = os.path.splitext(img_file)

        if '.jpg' not in img_file:
            continue

        img = imread(os.path.join(data_dir, img_file))
        cols, rows, _ = img.shape
        _img = np.zeros((image_size, image_size, 3))


        # Just need to pad
        if (cols > image_size) or (rows > image_size):
            resize_rat = image_size / np.max(img.shape[:2])

            img = rescale(img, resize_rat, mode='constant')
            cols, rows, _ = img.shape

        padding = abs(rows - image_size) // 2
        _img[:cols, padding:(rows + padding), :] = img

        try:
            y.append(np.loadtxt(os.path.join(data_dir, img_file.replace('jpg', 'txt'))))
            x.append(_img)
            filenames.append(basename)
            resize_ratios.append(resize_rat)
        except OSError:
            pass

    # Rescale etc
    x = np.asarray(x)
    x /= 255.

    return x, np.asarray(y), filenames, resize_ratios 

