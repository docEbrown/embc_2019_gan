#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Model training 


:author: Ben Johnston

"""
import numpy as np
import matplotlib.pyplot as plt
from keras import optimizers
from keras.models import Model
from keras.datasets import mnist
from keras.layers import Input
from src.models.generator import model1 as generator
from src.models.discriminator import model1 as discriminator
from src.data.gan_data import get_training_data 

_print = print

def print(msg):

    _print(msg)
    with open('progress.log', 'a') as f:
        f.write('%s\n' % msg)

def display_images(images, k):
    for i in range(images.shape[0]):
        plt.subplot(1, 8, i + 1)
        plt.imshow(images[i])
        plt.axis('off')
    plt.tight_layout()
    plt.savefig('%d.png' % k)
    plt.close('all')

# Seed
import numpy as np
np.random.seed(123)

import tensorflow as tf
tf.set_random_seed(123)

rand_size = 100 
img_size = 50 
d_learning_rate=0.0001
g_learning_rate=0.0000000000001
batch_size = 32 
n_iterations = 10000
n_epochs_print = 10

# Generator
gen_mod = generator(rand_size, img_size)
print('Generator')
print(gen_mod.summary())

gen_mod.compile(
    optimizer=optimizers.SGD(lr=g_learning_rate),
    loss='binary_crossentropy')

# Discriminator
disc_mod = discriminator(img_size)
print('Discriminator')
print(disc_mod.summary())

disc_mod.compile(
    optimizer=optimizers.SGD(lr=d_learning_rate),
    loss='binary_crossentropy')


# Define the GAN network
disc_mod.trainable = False
adv_input = Input(shape=(rand_size,), name='adv_in') 
adv_gen = gen_mod(adv_input)
adv_disc = disc_mod(adv_gen)
adv_mod = Model(adv_input, adv_disc)
print("GAN")
print(adv_mod.summary())

adv_mod.compile(
    optimizer=optimizers.SGD(lr=d_learning_rate),
    loss='binary_crossentropy')

data_gen = get_training_data(
    target_size=(img_size, img_size),
    batch_size=batch_size,
)

n_batches = len(data_gen.filenames) // batch_size
disc_rep = 1

for epoch in range(n_iterations):
    d_loss = 0
    g_loss = 0

    for batch in range(n_batches):
        x = data_gen.next()
        x = x.reshape((x.shape[0], -1))
        y = np.ones((x.shape[0] + batch_size)) * 0.9
        y[x.shape[0]:] = 0.1

        # Train Discriminator
        # Fake Data
        for _ in range(disc_rep):
            noise = np.random.uniform(-1, 1, size=(batch_size, rand_size))
            disc_mod.trainable = True
            z = gen_mod.predict(noise)
            # Real + Fake Data
            x_in = np.concatenate((x, z))
            d_loss += disc_mod.train_on_batch(x_in, y)

        # Train Generator
        noise = np.random.uniform(-1, 1, size=(batch_size, rand_size))
        y = np.ones((batch_size)) * 0.9
        disc_mod.trainable = False 
        g_loss += adv_mod.train_on_batch(noise, y)

    if epoch%n_epochs_print == 0:
        average_d_loss = d_loss / (n_batches * disc_rep)
        average_g_loss = g_loss / n_batches
        print('epoch: {0:04d}   d_loss = {1:0.6f}  g_loss = {2:0.6f}'
              .format(epoch,average_d_loss,average_g_loss))
        noise = np.random.uniform(-1, 1, size=(8, rand_size))
        x_pred = gen_mod.predict(noise)
        x_pred += abs(x_pred.min())
        x_pred /= x_pred.max()


        #display_images(x_pred, epoch)   
        display_images(x_pred.reshape(-1,img_size, img_size, 3), epoch)   


gen_mod.save('gen_mod.h5')
