#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Model training 


:author: Ben Johnston

"""
import numpy as np
import tensorflow as tf
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from src.models.landmark_models import model1
from src.data.data import LMRK_TRAIN_DIR, LMRK_VALID_DIR
from src.data.landmark_data import load_training_data

_print = print

def print(msg):

    _print(msg)
    with open('progress.log', 'a') as f:
        f.write('%s\n' % msg)

def flipxy(x, y, image_size):

    y[:,0] = (image_size // 2) - y[:,0]
    y[:,0] += (image_size // 2)
    return np.fliplr(x), y

# Seed
np.random.seed(0)
tf.set_random_seed(0)

image_size = 50
learning_rate = 0.001
momentum = 0.9

batch_size = 32 
n_iterations = 10000
n_epochs_print = 10
flip_rate = 0.5

# Generator
mod = model1(image_size) 

mod.compile(
    optimizer=optimizers.SGD(lr=learning_rate, momentum=momentum),
    loss='mse')

xtrain, ytrain = load_training_data(LMRK_TRAIN_DIR,
    image_size=image_size) 

print('%d training samples' % len(xtrain))
print(mod.summary())

xvalid, yvalid = load_training_data(LMRK_VALID_DIR,
    image_size=image_size) 
xvalid = xvalid.reshape((xvalid.shape[0], -1))

yvalid -= yvalid.mean()
yvalid /= yvalid.max()

yvalid = yvalid.reshape((yvalid.shape[0], -1))

data_gen = ImageDataGenerator().flow(
    x=xtrain, y=ytrain, batch_size=batch_size)

n_batches = len(xtrain) // batch_size

best_loss = np.inf
for epoch in range(n_iterations):
    loss = 0

    for batch in range(n_batches):
        x, y = data_gen.next()

        # Flip LR
        sample_idx = np.arange(len(x))
        np.random.shuffle(sample_idx)
        flip_idx = sample_idx[:int(len(x) * flip_rate)]

        for idx in flip_idx:
            x[idx], y[idx] = flipxy(x[idx], y[idx], image_size)

        y -= y.mean()
        y /= y.max()

        x = x.reshape((x.shape[0], -1))
        y = y.reshape((y.shape[0], -1))
        loss += mod.train_on_batch(x, y)

    average_loss = loss / n_batches
    if epoch % n_epochs_print == 0:
        valid_loss = mod.evaluate(x=xvalid, y=yvalid, batch_size=batch_size)
        print('epoch: %4d train = %0.6f valid = %0.6f' % (
            epoch, average_loss, valid_loss))

    if (average_loss < best_loss) and (epoch > 1000):
        best_loss = average_loss
        mod.save('mod.h5')
