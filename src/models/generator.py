#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""



:author: Ben Johnston

"""

# Imports
from keras.models import Sequential
from keras.layers import Dense, LeakyReLU, Reshape, Conv2D, UpSampling2D, Flatten


def model1(rand_size, img_size):

    g_model = Sequential()
    g_model.add(Dense(rand_size * 2, input_shape=(rand_size,), name='g0'))
    g_model.add(LeakyReLU())

    g_model.add(Dense(img_size ** 2 * 3, activation='tanh', name='g_end'))

    return g_model


def model2(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 3 * 3 * 32, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(3, 3, 32), input_shape=(3 * 3 * 32,)))

#    g_model.add(UpSampling2D(size=(2, 2)))
#    g_model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))
    return g_model


def model3(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 5 * 5 * 16, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(5, 5, 16), input_shape=(5 * 5 * 16,)))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))
    return g_model


def model4(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 5 * 5 * 128, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(5, 5, 128), input_shape=(5 * 5 * 128,)))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))

    g_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))

    return g_model


def model5(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 5 * 5 * 128, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(5, 5, 128), input_shape=(5 * 5 * 128,)))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))

    g_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))

    return g_model


def model6(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 3 * 3 * 128, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(3, 3, 128), input_shape=(3 * 3 * 128,)))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))

    g_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))

    return g_model


def model7(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 3 * 3 * 128, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(3, 3, 128), input_shape=(3 * 3 * 128,)))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))

    g_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))

    return g_model


def model7(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 3 * 3 * 128, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(3, 3, 128), input_shape=(3 * 3 * 128,)))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))

    g_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))

    return g_model


def model8(rand_size, img_size):

    g_model = Sequential()

    g_model.add(Dense(units = 3 * 3 * 128, input_shape=(rand_size,), activation='tanh'))
    g_model.add(Reshape(target_shape=(3, 3, 128), input_shape=(3 * 3 * 128,)))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(UpSampling2D(size=(2, 2)))
    g_model.add(Conv2D(filters=8, kernel_size=(3, 3), padding='same', activation='tanh'))

    g_model.add(Flatten())
    g_model.add(Dense(units=img_size * img_size * 3, activation='tanh'))

    g_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))

    return g_model
