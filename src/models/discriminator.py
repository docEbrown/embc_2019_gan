#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Descriminator model



:author: Ben Johnston

"""

# Imports
from keras.models import Sequential
from keras.layers import Dense, LeakyReLU, Dropout, Conv2D, MaxPooling2D,\
    Reshape, Activation, Flatten, Input

def model1(img_size):

    d_model = Sequential()

    d_model.add(Dense(1000, input_shape=(img_size ** 2 * 3,), name='d0'))
    d_model.add(LeakyReLU())
    d_model.add(Dropout(0.3))

    d_model.add(Dense(units=1, activation='sigmoid',name='d_out'))

    return d_model


def model2(img_size):

    d_model = Sequential()

    d_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))
    d_model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same'))
    d_model.add(LeakyReLU())
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Flatten())
    d_model.add(Dense(units=1, activation='sigmoid'))

    return d_model


def model3(img_size):

    d_model = Sequential()

    d_model.add(Reshape(target_shape=(img_size, img_size, 3), input_shape=(img_size * img_size * 3,)))
    d_model.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Flatten())
    d_model.add(Dense(units=1, activation='sigmoid'))

    return d_model


def model4(img_size):

    d_model = Sequential()

    d_model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3),  kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Flatten())
    d_model.add(Dense(units=1, activation='sigmoid'))

    return d_model


def model5(img_size):

    d_model = Sequential()

    d_model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3),  kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Flatten())
    d_model.add(Dense(units=1, activation='sigmoid'))

    return d_model


def model6(img_size):

    d_model = Sequential()

    d_model.add(Conv2D(filters=32, input_shape=(img_size, img_size, 3),  kernel_size=(3, 3), padding='same', activation='tanh'))
    d_model.add(MaxPooling2D(pool_size=(2,2), strides=(2,2)))

    d_model.add(Flatten())
    d_model.add(Dense(units=1, activation='sigmoid'))

    return d_model
