#! /usr/bin/env python
# -*- coding: utf-8 -*-
# S.D.G

"""Model training 


:author: Ben Johnston

"""
import os
import numpy as np
import tensorflow as tf
from keras import optimizers
from keras.preprocessing.image import ImageDataGenerator
from src.models.landmark_models import model8
from src.data.data import POST_TRAIN_DIR
from src.data.data import LMRK_TRAIN_DIR, LMRK_VALID_DIR
from src.data.landmark_data import load_training_data

LMRK_TRAIN_DIR = os.path.join(POST_TRAIN_DIR, 'training')
LMRK_VALID_DIR = os.path.join(POST_TRAIN_DIR, 'valid')

_print = print

def print(msg):

    _print(msg)
    with open('progress.log', 'a') as f:
        f.write('%s\n' % msg)


def flipxy(x, y):
    y[0], y[2] = y[2], y[0]
    return np.fliplr(x), y

# Seed
np.random.seed(0)
tf.set_random_seed(0)

image_size = 50
learning_rate = 0.000000002
momentum = 0.9
batch_size = 20 
n_iterations = 1000000
n_epochs_print = 10
flip_rate = 0.1
patience = 10000

# Generator
mod = model8(image_size) 
mod.load_weights('mod.h5')

# Freeze the first two layers of the model
for i in range(5):
    mod.layers[i].trainable = False

mod.compile(
    optimizer=optimizers.SGD(lr=learning_rate, momentum=momentum),
    loss='mse')

xtrain, ytrain, _, _ = load_training_data(LMRK_TRAIN_DIR,
    image_size=image_size) 

print('%d training samples' % len(xtrain))
print(mod.summary())

xvalid, yvalid, _, _ = load_training_data(LMRK_VALID_DIR,
    image_size=image_size) 
yvalid = yvalid.reshape((yvalid.shape[0], -1))

data_gen = ImageDataGenerator().flow(
    x=xtrain, y=ytrain, batch_size=batch_size)

n_batches = len(xtrain) // batch_size

best_loss = np.inf
best_epoch = 0
epoch = 0
while 1:
    loss = 0

    for batch in range(n_batches):
        x, y = data_gen.next()
        #x = x.reshape((x.shape[0], -1))
        y = y.reshape((y.shape[0], -1))

        # Flip LR
        sample_idx = np.arange(len(x))
        np.random.shuffle(sample_idx)
        flip_idx = sample_idx[:int(len(x) * flip_rate)]

        for idx in flip_idx:
            x[idx], y[idx] = flipxy(x[idx], y[idx])

        loss += mod.train_on_batch(x, y)

    average_loss = loss / n_batches
    if epoch % n_epochs_print == 0:
        valid_loss = mod.evaluate(x=xvalid, y=yvalid, batch_size=batch_size)
        print('epoch: %4d train = %0.6f valid = %0.6f' % (
            epoch, average_loss, valid_loss))

    if (average_loss < best_loss):
        best_loss = average_loss
        best_epoch = epoch
        mod.save('mod_transfer.h5')

    epoch += 1
